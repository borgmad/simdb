﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using SIMDB.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SIMDB.Models
{
    public class Movie
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [Display(Name = "Movie")]
        [Required(ErrorMessage = "Movie Title is required")]
        public string title { get; set; }

        [Display(Name = "Director")]
        [Required(ErrorMessage = "Name at least one director")]
        public string director { get; set; }

        [Display(Name = "Cast")]
        [Required(ErrorMessage = "Name at least one cast member")]
        public string actors { get; set; }

        [Display(Name = "Cover")]
        [Required(ErrorMessage = "Enter any image link to the movie cover")]
        [ImageMustBeLink(ErrorMessage = "Cover must be a link to an image!")]
        public string image { get; set; }

        [Display(Name = "Year")]
        [Required(ErrorMessage = "Year is required")]
        [Range(0, 2021, ErrorMessage = "You can only travel in time in movies")]
        public int year { get; set; }

        public UpdateDefinition<Movie> GetUpdateAllDefinitionForMovie()
        {
            return Builders<Movie>.Update
                .Set(x => x.title, title)
                .Set(x => x.director, director)
                .Set(x => x.actors, actors)
                .Set(x => x.image, image)
                .Set(x => x.year, year);
        }
    }
}