﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace SIMDB.Models
{
    public class MoviesDB
    {
        // Our web.config contains the Database name and Connectionstring 
        public string ConnectionString = ConfigurationManager.AppSettings["ConnectionString"];
        public string DatabaseName = ConfigurationManager.AppSettings["DatabaseName"];

        // Representing our database (movie)
        public IMongoDatabase Database;

        public MoviesDB()
        {
            var client = new MongoClient(ConnectionString);
            Database = client.GetDatabase(DatabaseName);
        }

        // Predefine our table of Movies in a MongoCollection
        public IMongoCollection<Movie> Movies
        {
            get
            {
                var movies = Database.GetCollection<Movie>("movie");

                return movies;
            }
        }
    }
}