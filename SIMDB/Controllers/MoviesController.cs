﻿using MongoDB.Bson;
using MongoDB.Driver;
using SIMDB.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SIMDB.Controllers
{
    public class MoviesController : Controller
    {
        // Database Context
        private readonly MoviesDB dbContext = new MoviesDB();

        // GET: Movies
        public ActionResult Index()
        {
            var movies = dbContext.Movies.Find(new BsonDocument()).ToList();

            return View(movies);
        }

        // GET: Movie
        public ActionResult Details(Movie movie)
        {
            var selectedMovie = dbContext.Movies.Find(Builders<Movie>.Filter.Eq("Id", movie.Id)).FirstOrDefault();

            return View(selectedMovie);
        }

        // Init a Movie creation Form
        public ActionResult Create()
        {
            return View();
        }

        // POST: Insert the new movie into DB
        [HttpPost]
        public ActionResult Create(Movie newMovie)
        {
            if(ModelState.IsValid)
            {
                dbContext.Movies.InsertOneAsync(newMovie);

                return RedirectToAction("Details", newMovie);
            }

            return View(newMovie);
        }

        [HttpGet]
        public ActionResult Edit(Movie movie)
        {
            if (movie.Id == null)
                return RedirectToAction("Index");

            var movieToEdit = dbContext.Movies.Find(x => x.Id == movie.Id).FirstOrDefault();

            return View(movieToEdit);
        }

        // POST: Update our edited Movie.
        // Note: added seperator because Edit GET also has to return the movie object (string returns null)
        //       otherwise the functions are identical and the controller does not like that.
        //       Not a great practise, but the string vs object problem could be specific to Mongo.
        [HttpPost]
        public ActionResult Edit(Movie movie, int? seperator = null)
        {
            if(ModelState.IsValid)
            {
                dbContext.Movies.UpdateOne(Builders<Movie>.Filter.Where(x => x.Id == movie.Id), movie.GetUpdateAllDefinitionForMovie());

                return RedirectToAction("Details", movie);
            }

            return RedirectToAction("Edit");           
        }

        [HttpGet]
        public ActionResult Delete(Movie movie)
        {
            if (movie.Id == null)
                return RedirectToAction("Index");

            var movieToDelete = dbContext.Movies.Find(Builders<Movie>.Filter.Eq("Id", movie.Id)).FirstOrDefault();

            return View(movieToDelete);
        }

        [HttpPost]
        public ActionResult Delete(Movie movie, int? seperator = null)
        {
            if (movie.Id == null)
                return RedirectToAction("Index");

            var movieDelete = dbContext.Movies.DeleteOne(Builders<Movie>.Filter.Where(x => x.Id == movie.Id));

            return RedirectToAction("Index");
        }
    }
}