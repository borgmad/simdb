﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SIMDB.Helpers
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class ImageMustBeLinkAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            var inputValue = value as string;
            var isValid = false;

            if(!string.IsNullOrEmpty(inputValue))
            {
                if (inputValue.ToUpperInvariant().Contains("HTTPS"))
                {
                    if (inputValue.ToUpperInvariant().Contains(".JPG") || inputValue.ToUpperInvariant().Contains(".PNG"))
                        isValid = true;
                };
            }

            return isValid;
        }
    }
}